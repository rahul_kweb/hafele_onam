GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OffersLeads](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](500) NULL,
	[Email] [nvarchar](500) NULL,
	[MobileNo] [nvarchar](500) NULL,
	[City] [nvarchar](500) NULL,
	[Offers] [nvarchar](max) NULL,
	[utm_source] [nvarchar](500) NULL,
	[utm_medium] [nvarchar](500) NULL,
	[utm_content] [nvarchar](500) NULL,
	[utm_campaign] [nvarchar](500) NULL,
	[Device] [nvarchar](500) NULL,
	[utm_city] [nvarchar](500) NULL,
	[AddedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Offers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SuperAdminLogin]    Script Date: 27-07-2021 13:09:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SuperAdminLogin](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Username] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Emailid] [nvarchar](50) NULL,
	[Mobileno] [varchar](15) NULL,
	[AddedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
	[Remark] [varchar](20) NULL,
	[UpdatedOn] [datetime] NULL,
	[LoginOn] [datetime] NULL,
	[LogoutOn] [datetime] NULL,
 CONSTRAINT [PK_SuperAdminLogin] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[SuperAdminLogin] ON 
GO
INSERT [dbo].[SuperAdminLogin] ([Id], [Name], [Username], [Password], [Emailid], [Mobileno], [AddedOn], [IsActive], [Remark], [UpdatedOn], [LoginOn], [LogoutOn]) VALUES (1, N'Hafele', N'admin', N'admin@123', N'shraddha.gurav@hafeleindia.com', N'1212121212', CAST(N'2018-07-17T00:00:00.000' AS DateTime), 1, N'#SuperAdmin', CAST(N'2021-06-07T12:09:49.623' AS DateTime), CAST(N'2021-07-19T20:37:09.803' AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[SuperAdminLogin] OFF
GO
/****** Object:  StoredProcedure [dbo].[Proc_OffersLeads]    Script Date: 27-07-2021 13:09:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_OffersLeads]
	@Para nvarchar(50)=null,
	@Id int=0,
	@FullName nvarchar(50)=null,
	@Email nvarchar(50)=null,
	@MobileNo nvarchar(50)=null,
	@City nvarchar(50)=null,
	@Offers nvarchar(50)=null,
	@utm_source nvarchar(50)=null,
	@utm_medium nvarchar(50)=null,
	@utm_content nvarchar(50)=null,
	@utm_campaign nvarchar(50)=null,
	@Device nvarchar(50)=null,
	@utm_city nvarchar(50)=null
AS
BEGIN
	If @Para ='add'
			Begin
				insert into OffersLeads
					(
						FullName,
						Email,
						MobileNo,
						City,
						Offers,
						utm_source,
						utm_medium,
						utm_content,
						utm_campaign,
						Device,
						utm_city,
						AddedOn,
						IsActive
					)
				values
					(
						@FullName,
						@Email,
						@MobileNo,
						@City,
						@Offers,
						@utm_source,
						@utm_medium,
						@utm_content,
						@utm_campaign,
						@Device,
						@utm_city,
						GETDATE(),
						1
					)
			End
		Else If @Para='get'
		Begin
			select 
			ROW_NUMBER() over (order by AddedOn desc)as Sr,
			Id,
			FullName,	
			Email,
			MobileNo,
			City,
			Offers,
			utm_source,
			utm_medium,
			utm_content,
			utm_campaign,
			Device,
			utm_city,
			CONVERT(varchar,AddedOn,106) as Date
		 from OffersLeads
		 where IsActive = 1
		 order by AddedOn desc	
		End

			Else If @Para='getExport'
		Begin
			select 
			ROW_NUMBER() over (order by AddedOn desc)as Sr,
			Id,
			FullName,	
			Email,
			MobileNo,
			City,
			Offers,
			utm_source,
			utm_medium,
			utm_content,
			utm_campaign,
			Device,
			utm_city,
			CONVERT(varchar,AddedOn,106) as Date
		 from OffersLeads
		 where IsActive = 1
		 order by AddedOn desc	
		End

	Else if @Para = 'delete'
	begin
		update OffersLeads set
		IsActive = 0,
		DeletedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'adminIndexCount'
	begin
		select COUNT(*) as Count,'OfferLeads' as Tbl from OffersLeads where IsActive = 1

	
	end
END

GO
/****** Object:  StoredProcedure [dbo].[Proc_SuperAdminLogin]    Script Date: 27-07-2021 13:09:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Proc_SuperAdminLogin]
(
	@PARA			NVARCHAR(100)=NULL,
	@NAME			NVARCHAR(100)=NULL,
	@USERNAME		NVARCHAR(100)=NULL,
	@PASSWORD		NVARCHAR(100)=NULL,
	@EMAILID		NVARCHAR(100)=NULL,
	@MOBILENO		VARCHAR(15)=NULL,
	@ID				INT=0,
	@NEWPASSWORD		NVARCHAR(100)=NULL
)
AS
BEGIN
	/*Note:- COLLATE SQL_Latin1_General_CP1_CS_AS is using for validating case sensitive*/
    IF @PARA='GET_LOGIN'
	BEGIN
	   IF EXISTS(SELECT * FROM SUPERADMINLOGIN 
				WHERE 
					USERNAME=@USERNAME COLLATE SQL_Latin1_General_CP1_CS_AS 
				AND [PASSWORD]=@PASSWORD COLLATE SQL_Latin1_General_CP1_CS_AS  
				AND ISACTIVE=1
				)
				BEGIN
					UPDATE SUPERADMINLOGIN SET LoginOn=getdate() --Update login time
					SELECT * FROM SUPERADMINLOGIN WHERE USERNAME=@USERNAME AND [PASSWORD]=@PASSWORD AND ISACTIVE=1 --fetch data after check valid uid & pwd
				END
		--ELSE
		--	BEGIN
		--		SELECT ''[incorrect]
		--	END
		
	END

	ELSE IF @PARA='CHANGE_PASSWORD'
	BEGIN
		IF EXISTS(SELECT * FROM SUPERADMINLOGIN 
				WHERE 
					USERNAME=@USERNAME COLLATE SQL_Latin1_General_CP1_CS_AS 
				AND [PASSWORD]=@PASSWORD COLLATE SQL_Latin1_General_CP1_CS_AS  
				AND ISACTIVE=1
			    )
				BEGIN
					UPDATE SUPERADMINLOGIN SET [Password]=@NEWPASSWORD,UpdatedOn=getdate() --Update New Pwd
					SELECT 'correct' [update]
				END		
			ELSE
			BEGIN
				SELECT 'incorrent'[update]
			END
	END

		ELSE IF @PARA='FORGOT_PASSWORD'	
		IF EXISTS(SELECT * FROM SUPERADMINLOGIN WHERE Emailid=@EMAILID AND ISACTIVE=1)
				BEGIN
					SELECT Name,Username,[Password],Emailid FROM SUPERADMINLOGIN WHERE Emailid=@EMAILID AND ISACTIVE=1 
				END		
			ELSE
			BEGIN
				SELECT ''[incorrect]
			END
		
		ELSE IF @PARA='GETDETAIL_FOR_UPDATE'	
			BEGIN
				SELECT Id,Name,Emailid,Mobileno FROM SUPERADMINLOGIN WHERE  ISACTIVE=1 
			END

		ELSE IF @PARA='UPDATEPROFILEDETAILS'
		begin
			UPDATE SUPERADMINLOGIN SET 
				UpdatedOn=getdate(),
				Name=@NAME,
				Emailid=@EMAILID,
				Mobileno=@MOBILENO
			where
				Id=@ID
		end

		ELSE IF @PARA='UPDATEOF_LOGOUT'
		begin
			UPDATE SUPERADMINLOGIN SET 
				LogoutOn=getdate()
			where
				Id=@ID
		end

		ELSE IF @PARA = 'ADMIN_TOTAL_COUNTS'
		BEGIN
			select '[' + STUFF((
				select 
					',{"Name":"Client","Count":' + CAST(COUNT(*) as varchar(max)) + ',"Url","/Admin/RegisteredUsers"},{"Name":"Category","Count":'+ cast((select top 1 COUNT(*) from Category where IsActive = 1) as varchar(max)) +',"Url":"/Admin/CategoryList"},{"Name":"SubCategory","Count":'+CAST((select top 1 COUNT(*) from SubCategory where IsActive = 1) as varchar(max))+',"Url":"/Admin/SubCategoryList"},{"Name":"Product","Count":'+ Cast((select top 1 COUNT(*) from Products where IsActive = 1) as varchar(max)) +',"Url":"/Admin/ProductList"}'
				from ClientRegistration
				for xml path(''), type
			).value('.', 'varchar(max)'), 1, 1, '') + ']' [TotalCounts]
		END
END











GO
