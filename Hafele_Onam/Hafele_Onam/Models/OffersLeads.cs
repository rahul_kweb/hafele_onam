﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hafele_Onam.Models
{
    public class OffersLeads
    {
        public string Sr { get; set; }
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string City { get; set; }
        public string Offers { get; set; }
        public string utm_source { get; set; }

        public string utm_medium { get; set; }

        public string utm_content { get; set; }
        public string utm_campaign { get; set; }

        public string Device { get; set; }
        public string utm_city { get; set; }
    
        public List<OffersLeads> OffersLeadsList { get; set; }

        public string Date { get; set; }

        public List<SelectListItem> OfferName { get; set; }
        public int[] OfferId { get; set; }

    }

  
}