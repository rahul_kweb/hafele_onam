﻿//using ClosedXML.Excel;
using ClosedXML.Excel;
using Hafele_Onam.Authentication;
using Hafele_Onam.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Hafele_Onam.Controllers
{
    [CustomAdminAccessFilter]
    public class AdminController : Controller
    {
        Utility util = new Utility();
        // GET: Admin
        public ActionResult Index()
        {
            var count = Counting();
            ViewBag.OffersLeads = count["OfferLeads"].ToString();
            return View();
        }

        public Dictionary<string, string> Counting()
        {
            Dictionary<string, string> objDic = new Dictionary<string, string>();
            StringBuilder strbuil = new StringBuilder();
            DataSet ds = new DataSet();
            ds = util.Display1("Exec Proc_OffersLeads 'adminIndexCount'");
            if (ds.Tables.Count > 0)
            {
                //OfferLeads
                objDic.Add(ds.Tables[0].Rows[0]["Tbl"].ToString(), ds.Tables[0].Rows[0]["Count"].ToString());

            }
            return objDic;
        }

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--- Admin Profile Section Start---xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult UpdateProfile()
        {
            DataTable dt = GetAdminDataForProfileUpdate();
            Login log = new Login();

            log.Id = dt.Rows[0]["Id"].ToString();
            log.Name = dt.Rows[0]["Name"].ToString();
            log.Emailid = dt.Rows[0]["Emailid"].ToString();
            log.Mobileno = dt.Rows[0]["Mobileno"].ToString();
            return View(log);
        }

        public DataTable GetAdminDataForProfileUpdate()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = util.Display("Execute Proc_SuperAdminLogin 'GETDETAIL_FOR_UPDATE'");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }

        [HttpPost]
        public JsonResult UpdateProfile(Login logobj)
        {
            var response = "";

            try
            {
                response = UpdateProfileDetails(logobj.Id, logobj.Name, logobj.Emailid, logobj.Mobileno);
            }
            catch
            {
                response = "";
            }
            string Url = string.Empty;
            string Responsetype = response == "" ? "Fail" : "Success";

            return Json(new { Responsetype = Responsetype }, JsonRequestBehavior.AllowGet);
        }

        public string UpdateProfileDetails(string id, string name, string emailid, string mobileno)
        {
            string Response = string.Empty;
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_SuperAdminLogin"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PARA", "UPDATEPROFILEDETAILS");
                    cmd.Parameters.AddWithValue("@NAME", name);
                    cmd.Parameters.AddWithValue("@EMAILID", emailid);
                    cmd.Parameters.AddWithValue("@MOBILENO", mobileno);
                    cmd.Parameters.AddWithValue("@ID", id);
                    if (util.Execute(cmd))
                    {
                        Response = "success";
                    }
                    else
                    {
                        Response = "";
                    }
                }
            }
            catch
            {

                Response = "";
            }
            return Response;
        }


        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ChangePassword(Login obj)
        {
            using (SqlCommand cmd = new SqlCommand("Proc_SuperAdminLogin"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PARA", "CHANGE_PASSWORD");
                cmd.Parameters.AddWithValue("@USERNAME", Request.Cookies["SuperAdminusername"].Value);
                cmd.Parameters.AddWithValue("@PASSWORD", obj.OldPassword);
                cmd.Parameters.AddWithValue("@NEWPASSWORD", obj.Password);
                DataTable dt = new DataTable();
                dt = util.Display(cmd);
                if (dt.Rows[0]["update"].ToString() == "correct")
                {
                    ViewBag.message = "Success";
                    ViewBag.message1 = "Your Password has been changed Successfully!";
                }
                else
                {
                    ViewBag.message = "fail";
                    ViewBag.message1 = "Sorry ! Password Could not be changed Successfully!";
                }
                ModelState.Clear(); //make blank to class property
            }
            return View();
        }

        #endregion xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--- Billing Profile Section End---xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


        #region xxxxxxxxxxxxxxxxxxxxxxxxxxx -- Offers Leads -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult Offers_Leads()
        {
            OffersLeads objOffersLeads = new OffersLeads();
            objOffersLeads.OffersLeadsList = OffersLeadsList();
            return View(objOffersLeads);
        }

        public List<OffersLeads> OffersLeadsList()
        {
            try
            {
                List<OffersLeads> objContactUs = new List<OffersLeads>();
                DataSet ds = new DataSet();
                ds = util.Display1("Execute Proc_OffersLeads 'get'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        OffersLeads obj = new OffersLeads();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.FullName = dr["FullName"].ToString();
                        obj.Email = dr["Email"].ToString();
                        obj.MobileNo = dr["MobileNo"].ToString();
                        obj.City = dr["City"].ToString();
                        obj.Offers = dr["Offers"].ToString();
                        obj.utm_source = dr["utm_source"].ToString();
                        obj.utm_medium = dr["utm_medium"].ToString();
                        obj.utm_content = dr["utm_content"].ToString();
                        obj.utm_campaign = dr["utm_campaign"].ToString();
                        obj.Device = dr["Device"].ToString();
                        obj.utm_city = dr["utm_city"].ToString();
                        obj.Date = dr["Date"].ToString();


                        objContactUs.Add(obj);

                    }
                }
                return objContactUs;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        public ActionResult DeleteOffers_Leads(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_OffersLeads"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Offers_Leads");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Offers_Leads");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("Offers_Leads");
                //throw;
            }

        }

        public ActionResult DownloadOfferListExcel()
        {
            try
            {
                List<OffersLeads> objList = new List<OffersLeads>();
                DataSet ds = new DataSet();
                ds = util.Display1("Exec Proc_OffersLeads 'getExport'");

                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(ds);
                    wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    wb.Style.Font.Bold = true;

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename= Offers_list.xlsx");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }

                }
                return RedirectToAction("Offers_Leads", "Admin");
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        #endregion
    }
}