﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Hafele_Onam
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
     name: "SuperAdmin",
     url: "SuperAdmin",
     defaults: new { controller = "Login", action = "Index", Usertype = "SuperAdmin" }

     );

            routes.MapRoute(
                       name: "Index",
                       url: "",
                       defaults: new { controller = "Home", action = "Index" }

                      );

            routes.MapRoute(
               name: "ThankYou",
               url: "ThankYou",
               defaults: new { controller = "Home", action = "ThankYou" }

              );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
